import sys

from PySide2 import QtWidgets

from widget import MainWindow

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    window = MainWindow("qt-logo.png")
    window.resize(900, 600)
    window.setMinimumSize(900, 600)
    window.show()

    sys.exit(app.exec_())
