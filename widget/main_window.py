from PySide2 import QtWidgets

from histogram import HistogramTask
from .histogram_view import HistogramView


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, image_path):
        super(MainWindow, self).__init__()

        self.__option = 1

        self.mainToolbar = self.addToolBar("mainToolbar")
        self.__init_toolbar()

        self.view = HistogramView()
        self.setCentralWidget(self.view)

        self.mainStatusBar = self.statusBar()
        self.__init_status_bar()

        self.task = None
        self.__image_path = image_path

        self.__load_image_data()

    def __init_toolbar(self):
        self.openImageButton = QtWidgets.QPushButton(self)
        self.openImageButton.setText("Open Image")
        self.openImageButton.clicked.connect(self.open_image)

        self.typeCombobox = QtWidgets.QComboBox(self)
        self.typeCombobox.addItems(["Line", "Fill"])
        self.typeCombobox.setCurrentIndex(self.__option)
        self.typeCombobox.activated.connect(self.selected_type)

        self.mainToolbar.addWidget(self.openImageButton)
        self.mainToolbar.addSeparator()
        self.mainToolbar.addWidget(QtWidgets.QLabel("Type: "))
        self.mainToolbar.addWidget(self.typeCombobox)

    def __init_status_bar(self):
        self.mainStatusBar.setLayout(QtWidgets.QHBoxLayout())
        self.msgLabel = QtWidgets.QLabel(self)
        # self.msgLabel.setText("<Message>")

        self.progressbar = QtWidgets.QProgressBar(self)
        self.progressbar.hide()

        self.mainStatusBar.addWidget(self.msgLabel)
        self.mainStatusBar.addWidget(self.progressbar, 1)

    def __load_image_data(self):
        self.msgLabel.setText("")
        self.__show_progress()

        self.task = HistogramTask(self.__image_path)
        self.task.finished.connect(self.finished_load_image_data)
        self.task.start()

    def __show_progress(self):
        self.progressbar.setMinimum(0)
        self.progressbar.setMaximum(0)
        self.progressbar.show()

    def __hide_progress(self):
        self.progressbar.setMinimum(1)
        self.progressbar.hide()

    # Slots
    def open_image(self):
        file_path, _ = QtWidgets.QFileDialog.getOpenFileName(
            self,
            "Open Image",
            "",
            "Select Images (*.jpg *.jpeg *.bmp)",
        )
        if len(file_path) == 0:
            return

        self.__image_path = file_path
        self.__load_image_data()

        del file_path

    def selected_type(self, index):
        self.view.set_paint_type(index)

    def finished_load_image_data(self):
        self.__hide_progress()
        self.msgLabel.setText("Image: " + self.__image_path)

        histogram_data = self.task.data
        self.view.set_data(histogram_data)

    # end-Slots
