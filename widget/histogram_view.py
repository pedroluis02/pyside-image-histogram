import math

from PySide2 import QtCore, QtGui, QtWidgets

from histogram import HistogramPaintType, length_2_points


class HistogramView(QtWidgets.QGraphicsView):

    def __init__(self):
        super(HistogramView, self).__init__()

        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

        self.scene = QtWidgets.QGraphicsScene(self)
        self.setScene(self.scene)

        self.paint_type = HistogramPaintType.FILL
        self.data = []

    def set_paint_type(self, paint_type):
        if self.paint_type.value == paint_type:
            return

        if paint_type == 0:
            self.paint_type = HistogramPaintType.LINE
        elif paint_type:
            self.paint_type = HistogramPaintType.FILL

        self.__draw()

    def set_data(self, data):
        self.data = data
        self.__draw()

    def draw(self):
        self.__draw()

    def __draw_axes(self):
        cw, ch = self.width(), self.height()

        x_left, x_right = 70, 10
        y_top, y_bottom = 10, 40

        self.__o = [x_left, ch - y_bottom]
        self.__px = [cw - x_right, ch - y_bottom]
        self.__py = [x_left, y_top]

        # create axis 'y'
        self.scene.addLine(self.__py[0], self.__py[1], self.__o[0], self.__o[1])

        # create axis 'x'
        self.scene.addLine(self.__o[0], self.__o[1], self.__px[0], self.__px[1])

        del cw, ch, x_left, x_right, y_top, y_bottom

    def __draw_histogram(self):
        hg = self.data
        max_y = max(hg)
        hg_array_length = 256 * 3

        self.scene.addText(str(max_y)).setPos(self.__py[0] - 50, self.__py[1])
        self.scene.addText("0").setPos(self.__o[0] - 20, self.__o[1])

        length_x = length_2_points(self.__o, self.__px) - 10
        length_y = length_2_points(self.__o, self.__py) - 10

        max_x = length_x / hg_array_length
        xa = self.__o[0] + 1
        ya = self.__o[1] - 1

        xex = xa
        it = 0

        aux_x = xa
        aux_y = ya

        for i in range(hg_array_length):
            x = xex
            xex = x + max_x
            y = ya - ((hg[int(i)] * length_y) / max_y)
            if i <= 255:
                color = QtGui.QColor(it, 0, 0)
            elif i <= 511:
                color = QtGui.QColor(0, it, 0)
            else:
                color = QtGui.QColor(0, 0, it)

            if self.paint_type == HistogramPaintType.LINE:
                self.scene.addLine(aux_x, aux_y, xex, math.trunc(y), QtGui.QPen(color))
                aux_x, aux_y = xex, math.trunc(y)
            elif self.paint_type == HistogramPaintType.FILL and hg[int(i)] > 0:
                rect = QtCore.QRectF()
                rect.adjust(x, ya, xex, math.trunc(y))
                self.scene.addRect(rect, QtGui.QPen(color), QtGui.QBrush(color))

            bottom_rect = QtCore.QRectF()
            bottom_rect.adjust(x, ya + 10, xex, ya + 30)
            self.scene.addRect(bottom_rect, QtGui.QPen(color), QtGui.QBrush(color))

            if it == 255 or it == 511:
                it = -1
            it += 1

        self.scene.update()

        del max_y, xa, ya, it, length_x, length_y, color

    def __draw(self):
        self.__clear()

        if len(self.data) == 0:
            return

        self.__draw_axes()
        self.__draw_histogram()

    def __clear(self):
        self.scene.clear()
        self.scene.update()

    def resizeEvent(self, event: QtGui.QResizeEvent):
        self.__draw()
