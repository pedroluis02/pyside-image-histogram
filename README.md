# PySide Image Histogram

Create RGB histogram UI from bitmap image using PySide (QT library for Python)

Libraries
----------
* PySide2
* Pillow (PIL)