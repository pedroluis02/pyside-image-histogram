from PIL import Image
from PySide2 import QtCore


class HistogramTask(QtCore.QThread):

    def __init__(self, image_path):
        QtCore.QThread.__init__(self)

        self.image_path = image_path
        self.data = []

    def run(self):
        self.__create_rgb_histogram()

    def __create_rgb_histogram(self):
        self.data = [0 for _ in range(256 * 3)]

        image = Image.open(self.image_path)
        (iw, ih) = image.size
        for i in range(iw):
            for j in range(ih):
                pixel = image.getpixel((i, j))
                r, g, b = pixel[0], pixel[1], pixel[2]
                self.data[r] += 1
                self.data[g + 256] += 1
                self.data[b + 512] += 1
