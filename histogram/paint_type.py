from enum import Enum


class HistogramPaintType(Enum):
    LINE = 0
    FILL = 1
