import math


def length_2_points(p1, p2):
    """
    Calculate length between two points

    :param p1: tuple(x1, y1)
    :param p2: tuple(x2, y2)
    :return: length
    """
    return math.sqrt(math.pow(p2[0] - p1[0], 2) + math.pow(p2[1] - p1[1], 2))
